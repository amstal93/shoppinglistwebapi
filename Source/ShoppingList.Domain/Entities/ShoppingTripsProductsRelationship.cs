namespace ShoppingList.Domain.Entities
{
    public class ShoppingTripsProductsRelationship
    {
        public int ProductId { get; set; }
        public Product Product { get; set; }

        public int ShoppingTripId { get; set; }
        public ShoppingTrip ShoppingTrip { get; set; }
    }
}
