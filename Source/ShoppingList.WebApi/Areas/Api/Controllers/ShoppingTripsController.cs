using System.Threading.Tasks;
using AutoMapper;
using Halcyon.HAL;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using ShoppingList.Application.CQRS.ShoppingTrips.Queries.GetAllShoppingTrips;

namespace ShoppingList.WebUI.Areas.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Area("api")]
    [Route("[controller]")]
    public class ShoppingTripsController : ControllerBase
    {

        private readonly IMapper m_mapper;
        private readonly IMediator m_mediator;
        private readonly LinkGenerator m_linkgen;

        public ShoppingTripsController(IMapper mapper, IMediator mediator, LinkGenerator linkgen)
        {
            m_mapper = mapper;
            m_mediator = mediator;
            m_linkgen = linkgen;
        }

        [HttpGet("", Name = nameof(GetShoppingTripCollection))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetShoppingTripCollection()
        {
            var query = new GetAllShoppingTripsQuery();

            var result = await m_mediator.Send(query);

            if (result.IsFailure)
            {
                return NotFound();
            }

            var resource = result.Success.Value;

            // return Ok(value: responseResource);
            var halResponse = new HALResponse(null)
                .AddLinks(
                    new Link[]
                    {
                        new Link("self", Request.Path),
                            new Link("up", m_linkgen.GetPathByAction(HttpContext, action : nameof(RootController.Get), controller: "Root"))
                    }
                )
                .AddEmbeddedCollection("shoppingtrips", resource);

            return Ok(halResponse);
        }

    }
}
