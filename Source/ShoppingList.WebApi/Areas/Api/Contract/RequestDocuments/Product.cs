using FluentValidation;

namespace ShoppingList.WebUI.Areas.Api.Contract.RequestDocuments
{
    public class Product
    {
        public string Name { get; set; }
    }

    public class ProductValidator : AbstractValidator<Product>
    {
        public ProductValidator()
        {
            RuleFor(x => x.Name)
                .NotNull()
                .MinimumLength(2)
                .MaximumLength(64);
        }
    }

}
