using System.Collections.Generic;
using AutoMapper;

namespace ShoppingList.WebUI.Areas.Api.Contract.Resources
{
    using CQRS = Application.CQRS;

    public class MappingProfile : Profile
    {

        public MappingProfile()
        {
            CreateMap<CQRS.Products.Queries.GetProductById.ProductDto, Contract.Resources.ProductItem>();
            CreateMap<CQRS.Products.Queries.GetProductById.ProductDto, Contract.Resources.Product>();

            CreateMap<CQRS.Products.Queries.GetAllProducts.ProductDto, Contract.Resources.Product>();
            CreateMap<CQRS.Products.Queries.GetAllProducts.ProductDto, Contract.Resources.ProductItem>();
            CreateMap<List<CQRS.Products.Queries.GetAllProducts.ProductDto>, Contract.Resources.ProductCollection>()
                .ForMember(dest => dest.Products, opt => opt.MapFrom(src => src));

            CreateMap<Contract.RequestDocuments.Product, CQRS.Products.Commands.CreateProduct.CreateProductCommand>();
            CreateMap<Contract.RequestDocuments.Product, CQRS.Products.Commands.UpdateProductById.UpdateProductByIdCommand>();

            CreateMap<CQRS.ShoppingTrips.Queries.GetAllShoppingTrips.ShoppingTripDto, Resources.ShoppingTrip>();
        }
    }
}
