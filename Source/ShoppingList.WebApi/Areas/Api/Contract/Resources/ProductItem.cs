namespace ShoppingList.WebUI.Areas.Api.Contract.Resources
{
    public class ProductItem
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
