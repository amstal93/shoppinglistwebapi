using System;
using System.Collections.Generic;

namespace ShoppingList.WebUI.Areas.Api.Contract.Resources
{
    public class ShoppingTrip
    {
        public DateTime DateTime { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}
