namespace ShoppingList.Application.Infrastructure
{
    public class AutoMapperProfile : AutoMapper.Profile
    {

        public AutoMapperProfile()
        {
            CreateMap<Domain.Entities.Product, CQRS.Products.Queries.GetAllProducts.ProductDto>().ReverseMap();
            CreateMap<Domain.Entities.Product, CQRS.Products.Queries.GetProductById.ProductDto>().ReverseMap();

            CreateMap<Domain.Entities.Product, CQRS.ShoppingTrips.Queries.GetAllShoppingTrips.ProductDto>().ReverseMap();
            CreateMap<Domain.Entities.ShoppingTrip, CQRS.ShoppingTrips.Queries.GetAllShoppingTrips.ShoppingTripDto>().ReverseMap();
        }

    }
}
