using System.Collections.Generic;
using Maciel.Monads;
using MediatR;

namespace ShoppingList.Application.CQRS.ShoppingTrips.Queries.GetAllShoppingTrips
{

    using QueryResult = Result<ErrorCode, List<ShoppingTripDto>>;

    public enum ErrorCode
    {
        Unknown
    }

    public class GetAllShoppingTripsQuery : IRequest<QueryResult>
    {

    }
}
