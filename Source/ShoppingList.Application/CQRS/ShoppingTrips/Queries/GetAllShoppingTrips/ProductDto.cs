using System.Collections.Generic;
using Maciel.Monads;

namespace ShoppingList.Application.CQRS.ShoppingTrips.Queries.GetAllShoppingTrips
{
    public class ProductDto
    {
        public int ProductId { get; set; }

        public string Name { get; set; }
    }
}
