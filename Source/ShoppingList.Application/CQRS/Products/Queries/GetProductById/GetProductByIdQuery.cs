﻿using Maciel.Monads;
using MediatR;

namespace ShoppingList.Application.CQRS.Products.Queries.GetProductById
{
    using QueryResult = Result<ErrorCode, CQRS.Products.Queries.GetProductById.ProductDto>;

    public enum ErrorCode : byte
    {
        NotFound,
        Unknown
    }

    public class GetProductByIdQuery : IRequest<QueryResult>
    {
        public int ProductId { get; set; }
    }
}
