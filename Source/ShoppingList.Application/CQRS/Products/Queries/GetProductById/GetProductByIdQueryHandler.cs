using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Maciel.Monads;
using MediatR;
using ShoppingList.Application.Interfaces.Repositories;

namespace ShoppingList.Application.CQRS.Products.Queries.GetProductById
{
    using QueryResult = Result<ErrorCode, CQRS.Products.Queries.GetProductById.ProductDto>;

    public class GetProductByIdQueryHandler : IRequestHandler<GetProductByIdQuery, QueryResult>
    {
        private readonly IProductRepository m_productRepository;
        private readonly IMapper m_mapper;

        public GetProductByIdQueryHandler(IProductRepository productRepository, IMapper mapper)
        {
            m_productRepository = productRepository;
            m_mapper = mapper;
        }

        public async Task<QueryResult> Handle(GetProductByIdQuery request, CancellationToken cancellationToken)
        {
            var response = await m_productRepository.FindById(request.ProductId, cancellationToken);

            if (response.IsFailure)
            {
                switch (response.Failure.Value)
                {
                    case Interfaces.Repositories.ErrorCode.NotFound:
                        return QueryResult.MakeFailure(ErrorCode.NotFound);

                    default:
                        return QueryResult.MakeFailure(ErrorCode.Unknown);
                }
            }

            return QueryResult.MakeSuccess(m_mapper.Map<ProductDto>(response.Success.Value));
        }
    }
}
