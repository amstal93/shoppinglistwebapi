using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Maciel.Monads;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ShoppingList.Application.Interfaces.Repositories;

namespace ShoppingList.Application.CQRS.Products.Queries.GetAllProducts
{
    using QueryResult = Result<ErrorCode, List<ProductDto>>;

    using OutputType = List<ProductDto>;

    public class GetAllProductsQueryHandler : IRequestHandler<GetAllProductsQuery, QueryResult>
    {

        private readonly IProductRepository m_repository;
        private readonly IMapper m_mapper;

        public GetAllProductsQueryHandler(IProductRepository repository, IMapper mapper)
        {
            m_repository = repository;
            m_mapper = mapper;
        }

        public async Task<QueryResult> Handle(GetAllProductsQuery request, CancellationToken cancellationToken)
        {
            var result = await m_repository.GetAll(cancellationToken);

            if (result.IsFailure)
            {
                return QueryResult.MakeFailure(ErrorCode.Unknown);
            }

            var response = m_mapper.Map<OutputType>(result.Success.Value);

            return QueryResult.MakeSuccess(response);
        }
    }

}
