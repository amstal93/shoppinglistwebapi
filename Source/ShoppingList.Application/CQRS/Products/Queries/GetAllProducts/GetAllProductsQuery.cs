using System.Collections.Generic;
using Maciel.Monads;
using MediatR;

namespace ShoppingList.Application.CQRS.Products.Queries.GetAllProducts
{
    using QueryResult = Result<ErrorCode, List<ProductDto>>;

    public enum ErrorCode
    {
        Unknown
    }

    public class GetAllProductsQuery : IRequest<QueryResult>
    {

    }
}
