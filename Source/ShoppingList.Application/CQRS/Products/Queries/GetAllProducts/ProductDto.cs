using System.Collections.Generic;
using Maciel.Monads;

namespace ShoppingList.Application.CQRS.Products.Queries.GetAllProducts
{
    public class ProductDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

}
