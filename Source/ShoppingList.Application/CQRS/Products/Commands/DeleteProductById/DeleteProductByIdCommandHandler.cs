using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Maciel.Monads;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ShoppingList.Application.Interfaces.Repositories;

namespace ShoppingList.Application.CQRS.Products.Commands.DeleteProductById
{
    using CommandResult = Result<ErrorCode, Nothing>;

    public class DeleteProductByIdCommandHandler : IRequestHandler<DeleteProductByIdCommand, CommandResult>
    {
        private readonly IProductRepository m_repository;
        private readonly DbContext m_context;
        private readonly IMapper m_mapper;

        public DeleteProductByIdCommandHandler(IProductRepository repository, DbContext context, IMapper mapper)
        {
            m_repository = repository;
            m_context = context;
            m_mapper = mapper;
        }

        public async Task<CommandResult> Handle(DeleteProductByIdCommand request, CancellationToken cancellationToken)
        {
            var response = await m_repository.Delete(request.ProductId, cancellationToken);

            if (response.IsFailure)
            {
                switch (response.Failure.Value)
                {
                    case Interfaces.Repositories.ErrorCode.NotFound:
                        return new Failure<ErrorCode>(ErrorCode.NotFound);

                    default:
                        return new Failure<ErrorCode>(ErrorCode.Unknown);
                }
            }

            return CommandResult.MakeSuccess(new Nothing());
        }
    }
}
