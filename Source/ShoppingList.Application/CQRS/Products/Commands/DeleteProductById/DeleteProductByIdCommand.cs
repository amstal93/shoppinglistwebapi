using Maciel.Monads;
using MediatR;

namespace ShoppingList.Application.CQRS.Products.Commands.DeleteProductById
{
    using CommandResult = Result<ErrorCode, Nothing>;

    public enum ErrorCode
    {
        NotFound,
        Unknown
    }

    public class DeleteProductByIdCommand : IRequest<CommandResult>
    {
        public int ProductId { get; set; }
    }
}
