using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ShoppingList.Application.Interfaces.Repositories;
using ShoppingList.Domain.Entities;
using ShoppingList.Persistence.Repositories;
using Xunit;

namespace ShoppingList.Persistence.Tests
{
    public class TestShoppingTripRepository : IDisposable
    {

        private readonly SqliteConnection _connection;
        private readonly ShoppingListDbContext _dbContext;
        private IShoppingTripRepository Repository { get; }

        public TestShoppingTripRepository()
        {
            _connection = new SqliteConnection("DataSource=:memory:");
            _connection.Open();

            var options = new DbContextOptionsBuilder<ShoppingListDbContext>()
                .UseSqlite(_connection)
                .Options;

            // Create the schema in the database
            _dbContext = new ShoppingListDbContext(options);
            _dbContext.Database.EnsureCreated();

            Repository = new ShoppingTripRepository(_dbContext);
        }

        [Fact]
        public async Task Given_EmptyRepository_When_GetAll_Then_ReturnEmptyList()
        {
            //Given

            //When
            var entities = await Repository.GetAll();

            //Then
            Assert.True(entities.IsSuccess);
            Assert.Empty(entities.Success.Value);
        }

        [Fact]
        public async Task Given_EmptyRepository_When_FindByIdAnyProduct_Then_ReturnNull()
        {
            //Given
            int someId = 0;

            //When
            var response = await Repository.FindById(someId, new CancellationToken() );
             
            //Then
            Assert.True(response.IsFailure);
        }

        [Fact]
        public async Task Given_EmptyRepository_When_Add_Then_ResultIsSuccess()
        {
            //Given
            var entity = new ShoppingTrip
            {
                Date = DateTime.Now
            };

            //When
            var result = await Repository.Create(entity, new CancellationToken());
             
            //Then
            Assert.True(result.IsSuccess);
        }

        [Fact]
        public async Task Given_EmptyRepository_When_AddNewEntity_Then_ResultEntityDateIsEqualToEntityDate()
        {
            //Given
            var inputEntity = new ShoppingTrip
            {
                Date = DateTime.Now
            };

            //When
            var result = await Repository.Create(inputEntity, new CancellationToken());
             
            //Then
            Assert.True(result.IsSuccess);
            Assert.Equal(inputEntity.Date, result.Success.Value.Date);
        }

        public void Dispose()
        {
            _connection.Close();
        }
    }
}
