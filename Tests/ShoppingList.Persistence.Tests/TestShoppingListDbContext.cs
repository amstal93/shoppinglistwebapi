using System;
using System.Linq;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace ShoppingList.Persistence.Tests
{
    public class TestShoppingListDbContext: IDisposable
    {
        private readonly SqliteConnection _connection;
        private readonly ShoppingListDbContext _dbContext;
        
        public TestShoppingListDbContext()
        {
            _connection = new SqliteConnection("DataSource=:memory:");
            _connection.Open();

            var options = new DbContextOptionsBuilder<ShoppingListDbContext>()
                .UseSqlite(_connection)
                .Options;

            // Create the schema in the database
            _dbContext = new ShoppingListDbContext(options);
            _dbContext.Database.EnsureCreated();
        }

        [Fact]
        public void Given_DbContext_When_init_Then_ProductsDbSetIsEmpty()
        {
            //Given
            //When

            //Then
            Assert.Equal(0, _dbContext.Products.Count());
        }

        [Fact]
        public void Given_DbContext_When_init_Then_ShoppingTripsDbSetIsEmpty()
        {
            //Given
            //When

            //Then
            Assert.Equal(0, _dbContext.ShoppingTrips.Count());
        }

        [Fact]
        public void Given_DbContext_When_init_Then_ShoppingTripsProductsRelationshipDbSetIsEmpty()
        {
            //Given
            //When

            //Then
            Assert.Equal(0, _dbContext.ShoppingTripsProductsRelationship.Count());
        }

        [Fact]
        public void Given_DbContext_When_AddProduct_Then_ProductsDbSetIsNonEmpty()
        {
            //Given

            //When
            _dbContext.Products.Add(new Domain.Entities.Product{Name = "First"});
            _dbContext.SaveChanges();

            //Then
            Assert.True(_dbContext.Products.Count() > 0);
        }

        public void Dispose()
        {
            _connection.Close();
        }
    }
}
