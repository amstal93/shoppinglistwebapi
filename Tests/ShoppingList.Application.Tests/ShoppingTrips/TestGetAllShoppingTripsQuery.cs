using System;
using System.Reflection;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ShoppingList.Application.CQRS.ShoppingTrips.Queries.GetAllShoppingTrips;
using ShoppingList.Application.Interfaces.Repositories;
using ShoppingList.Persistence;
using ShoppingList.Persistence.Repositories;
using Xunit;

namespace ShoppingList.Application.Tests
{
    public class TestGetAllShoppingTripsQuery
    {

        private IServiceProvider ServiceProvider { get; }

        public TestGetAllShoppingTripsQuery()
        {
            ServiceProvider = new ServiceCollection()
                .AddScoped<DbContext, ShoppingListDbContext>()
                .AddDbContext<ShoppingListDbContext>(
                    optionsAction: options => options.UseInMemoryDatabase(databaseName: "shopping_list_database")
                )
                .AddAutoMapper(typeof(Application.Infrastructure.AutoMapperProfile).GetTypeInfo().Assembly)
                .AddScoped<IMediator, Mediator>()
                .AddScoped<IShoppingTripRepository, ShoppingTripRepository>()
                .AddMediatR(typeof(Application.CQRS.Products.Queries.GetProductById.GetProductByIdQueryHandler).GetTypeInfo().Assembly)
                .BuildServiceProvider();
        }

        [Fact]
        public async System.Threading.Tasks.Task TestHappyPath()
        {
            //Given
            var command = new GetAllShoppingTripsQuery();

            //When
            var mediator = (IMediator)ServiceProvider.GetService(typeof(IMediator));
            var response = await mediator.Send(command);

            //Then
            Assert.True(response.IsSuccess);
        }
    }
}
